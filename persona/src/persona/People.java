package persona;


public class People {

    public static void main(String[] args) {
        
        //Imprimir 1er. Persona
        System.out.println("->1er. Persona: ");
        
        Persona1 p1 = new Persona1() {};
        
        //Imprimir informacion de la 1er. Persona
        
        p1.imprimirInformacion();
        
        //------------------------------------------- 
        
        //Imprimir 2da. Persona
        System.out.println("->2da. Persona: ");
        
        Persona2 p2 = new Persona2() {};
        
        //Imprimir informacion de la 1er. Persona
        
        p2.imprimirInformacion();
        
        //------------------------------------------- 
    }
    
    public static abstract class Persona {

        // Nombre de la Persona.

        private String nombre;

        // Nombre de su Ocupacion.

        private String ocupacion;

        
// Constructor predeterminado de la clase.

        public Persona() {
            /*
            Este constructor por defecto esta vacio, puede utilizarse
            para la inicializacion de componentes de la clase, se demostrara
            su uso en las subclases que hereden a esta clase.
            */
        }
        
        public Persona(String nombre, String ocupacion)
                          {
            this.nombre = nombre;

            this.ocupacion = ocupacion;

        }
        
        /*
        Declaracion de los getters y setters
        de cada variable miembro de la clase.
        */
        
        public String getNombre() {
            return this.nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }


        public String getOcupacion() {
            return this.ocupacion;
        }

        public void setOcupacion(String ocupacion) {
            this.ocupacion = ocupacion;
        }
        
         /*
        Metodo para imprimir la informacion
        de la persona.
        */
        
        public void imprimirInformacion() {
            System.out.println("");

            System.out.println("Nombre: " + nombre);

            System.out.println("Ocupacion: " + ocupacion);


            System.out.println("");
        }
       
        /*
        Metodo abstracto que obtiene la informacion
        general de la persona.
        */

        abstract String getPersonaInformacion();
    
}
     /*
    Declaracion de la clase Persona1 que hereda todos los
    atributos de la superclase Asignatura mediante: extends Persona
    en su declaración.
    */

      public static class Persona1 extends Persona {

        public Persona1() {
            /*
            Inicializacion de la clase Persona2 mediante
            el constructor sobrecargado definido en la superclase Persona
            */

            super("Will Traynor ", "Deportista");
        }


        public String getPersonaInformacion() {
            return "";
        }

    }
    
     /*
    Declaracion de la clase Persona2 que hereda todos los
    atributos de la superclase Persona mediante: extends Persona
    en su declaración.
    */

    public static class Persona2 extends Persona {

        public Persona2() {
            /*
            Inicializacion de la clase Persona2 mediante
            el constructor sobrecargado definido en la superclase Persona
            */

            super("Louisa Clark ", "Estudiante");
        }


        public String getPersonaInformacion() {
            return "";
        }

    }
}
